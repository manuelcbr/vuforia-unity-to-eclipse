##Vuforia AR app in Unity to Eclipse

***
This is a simple guide to export vuforia-Unity app to Eclipse properly. The goal is that you can add android code over unity projects

***

* Unity -> Edit -> Proyect Settings -> Player. Change Bundle Identifier and Minimum API Level (2.2 Froyo)

* Unity -> Build settings -> Check "Create Eclipse project" -> Export ->**Dont** create new folder in eclipse workspace -> Save

* (It´s not necesary for the last version of the Vuforia plugin) Copy the “/QCAR” folder (which contains your Dataset .XML/.DAT files) from the “/raw” directory of the “StagingArea” directory of your Unity project (\Project\Temp\StagingArea\raw\QCAR ) to the “/assets” folder of the Workspace project.

* In eclipse: File -> Import -> Existing android code -> copy in Workspace

* Right click in your project -> Properties -> Java Build Path -> select Order and Export tab and tick (enable the checkboxes) for the following:
 
 1. Android 4.0
 
 2. Android Dependencies
 
 3. classes.jar

* Properties -> Java Compiler -> 1.6

* Rigth click -> Android tools -> Fix proyect properties

* Run